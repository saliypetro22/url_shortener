import abc
from pymongo import MongoClient

from app.adapters import repository
from app.config import settings


class AbstractUnitOfWork(abc.ABC):
    urls: repository.AbstractRepository

    def __enter__(self):
        return self

    def __exit__(self, *args):
        return NotImplementedError


class MongoDbUnitOfWork(AbstractUnitOfWork):
    def __init__(self, session_factory=MongoClient(settings.MONGODB_URI)):
        self.session_factory = session_factory

    def __enter__(self):
        self.session = self.session_factory
        self.urls = repository.MongoDbRepository(self.session)
        return super().__enter__()

    def __exit__(self, *args):
        super().__exit__(*args)
