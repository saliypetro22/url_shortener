import datetime


def get_expiry_datetime(days: int):
    """
        Method returns datetime via a given number of days from now
    """
    utc_time = datetime.datetime.utcnow()
    expiry_datetime = utc_time + datetime.timedelta(days)

    return expiry_datetime
