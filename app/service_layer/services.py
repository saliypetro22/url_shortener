from app.service_layer import unit_of_work
from app.service_layer.utils import get_expiry_datetime


def add_url_to_database(long_url: str, short_code: str, ttl: int,
                        uow: unit_of_work.AbstractUnitOfWork):

    timestamp = get_expiry_datetime(ttl)
    document = {
        "long_url": long_url,
        "short_code": short_code,
        "expireAt": timestamp
    }

    with uow:
        uow.urls.create_index()
        uow.urls.add(document=document)


def get_url_from_database(short_code: str, uow: unit_of_work.AbstractUnitOfWork):
    with uow:
        document = uow.urls.get(short_code=short_code)
        return document
