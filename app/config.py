import os

from pydantic import BaseSettings


class Settings(BaseSettings):
    BASE_URL = os.getenv('BASE_URL', default='http://localhost:8000')
    APP_PORT = os.getenv('PORT', default=8000)

    MONGODB_URI = os.getenv('MONGODB_URI', default='mongodb://localhost:27017')

    DATABASE_NAME = 'urls'
    COLLECTION_NAME = 'urls_collection'
    INDEX_FOR_EXPIRATION = "expireAt"


settings = Settings()
