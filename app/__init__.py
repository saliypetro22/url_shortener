from fastapi import FastAPI

from app.entrypoints.router import url_shortener_router, url_redirect_router
from app.config import settings


app = FastAPI(title="URL Shortener",
              description="API Documentation")


app.include_router(url_shortener_router)
app.include_router(url_redirect_router)
