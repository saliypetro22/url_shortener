import socket
import uvicorn

from app.config import settings


if __name__ == "__main__":
    host = socket.gethostbyname(socket.gethostname())

    uvicorn.run("app.__init__:app", host=host, port=int(settings.APP_PORT))
