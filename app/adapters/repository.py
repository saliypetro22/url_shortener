import abc

from app.config import settings


class AbstractRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, document: dict):
        raise NotImplementedError

    @abc.abstractmethod
    def get(self, short_code: str):
        raise NotImplementedError


class MongoDbRepository(AbstractRepository):
    def __init__(self, session):
        self.session = session

    def get_collection(self):
        database = self.session[settings.DATABASE_NAME]
        collection = database[settings.COLLECTION_NAME]
        return collection

    def add(self, document: dict):
        collection = self.get_collection()
        collection.insert_one(document)

    def get(self, short_code: str):
        collection = self.get_collection()
        document = collection.find_one({"short_code": short_code})
        return document

    def create_index(self):
        collection = self.get_collection()

        if settings.INDEX_FOR_EXPIRATION in collection.list_indexes():
            collection.create_index(settings.INDEX_FOR_EXPIRATION, expireAfterSeconds=0)

