from fastapi import HTTPException
from pydantic import BaseModel, validator
import validators


class URLCreateModel(BaseModel):
    """
        Model for HTTP message body for create_url API
    """
    long_url: str
    ttl: int = 90

    @validator('long_url')
    def validate_url(cls, v):
        if not validators.url(v):
            raise HTTPException(status_code=400, detail="Your provided URL is not valid")
        return v

    @validator('ttl')
    def validate_TTL(cls, v):
        if not 1 <= v <= 365:
            raise HTTPException(status_code=422, detail="Incorrect TTL. Please enter days in range 1 to 365")
        return v


class URLCreateModelResponse(BaseModel):
    """
        Model for response of create_url API
    """
    long_url: str
    short_url: str
    ttl: int

    class Config:
        schema_extra = {
            "example": {
                "long_url": "https://www.youtube.com/loooong-url",
                "short_url": "http://localhost/short-url",
                "TTL": 30
            }
        }
