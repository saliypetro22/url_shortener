import os
import shortuuid

from fastapi import APIRouter, HTTPException
from fastapi.responses import RedirectResponse

from app.domain.models import URLCreateModel, URLCreateModelResponse
from app.service_layer import unit_of_work
from app.config import settings
from app.service_layer.services import add_url_to_database, get_url_from_database


url_shortener_router = APIRouter(prefix="/api")
url_redirect_router = APIRouter()


@url_shortener_router.post("/create/", response_model=URLCreateModelResponse,
                           summary="Create short URL")
async def create_url(shortener: URLCreateModel):
    """
    Creates a short URL for the provided long URL and stores both in a MongoDB database.

     **:param shortener:** model, which has two fields: long_url and TTL
    - **long_url**: any existing URL
    - **TTL**: time, which short_url will live

    **:return:** URLCreateModelResponse with long_url, associated short_url and TTL for short_url
    """

    long_url = shortener.long_url
    ttl = shortener.ttl

    short_code = shortuuid.ShortUUID().random(length=8)
    short_url = f"{settings.BASE_URL}/{short_code}"

    try:
        add_url_to_database(long_url=long_url, short_code=short_code, ttl=ttl,
                            uow=unit_of_work.MongoDbUnitOfWork())
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An unknown error occurred - {e}")

    response = URLCreateModelResponse(long_url=long_url, short_url=short_url, ttl=ttl)
    return response


@url_redirect_router.get("/{short_code}/", summary="URL Redirect")
async def url_redirect(short_code: str):
    """
    This API redirects from previously created short URL to the real one.

    **:param short_code:** code for short_url

    **:return:** redirecting to existing and previously defined URL
    """

    try:
        document_with_urls = get_url_from_database(short_code=short_code,
                                                   uow=unit_of_work.MongoDbUnitOfWork())
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An unknown error occurred - {e}")

    if not document_with_urls:
        raise HTTPException(status_code=404, detail="URL not found !")

    long_url = document_with_urls.get('long_url')

    response = RedirectResponse(url=long_url)

    return response
