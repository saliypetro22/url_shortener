------------------------------------------------------------------------------------------------------------------------
>                                       **URL SHORTENER PROJECT CODE BASIC DOCUMENTATION**
------------------------------------------------------------------------------------------------------------------------
### PROJECT OVERVIEW

This service creates short links with redirection to the real ones.

Project consist of two APIs:
  + Create_URL - creates short links
  + URL_Redirect - redirects short links to the real 


### PRODUCTION ENVIRONMENT

In case you want to test production mode of the project, follow the link to [Swagger](https://url-shortener-task.herokuapp.com/docs).

To test service:
  + Open create API and click on "Try now".
  + Enter existing URL address instead of "string". Also, you may change TTL in range 1 to 365 days.
  + Get the response from server below and copy short_url.
  + Paste short_url to browser line, and you will be redirected to the real website.

[NOTICE] Please notice that you can create URL in Swagger, however, it doesn't support redirection, you may only see the
status_code in DevTools/Network. Please enter the short URL in browser line to test it.


### LOCAL ENVIRONMENT

To run the application locally, follow these steps:
  + Clone the repository ```git clone https://gitlab.com/saliypetro22/url_shortener.git```
  + Create virtual environment and activate it
  + Install requirements for project ```pip install -r requirements.txt```
  + Install MongoDB locally following [guide](https://www.mongodb.com/docs/manual/installation/)
  + Run command ```python3 wsgi.py``` or ```python wsgi.py```
